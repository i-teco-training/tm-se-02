package ru.alekseev.tm;

import java.util.ArrayList;
import java.util.List;

public class ProjectManager {

    private List<Project> projects = new ArrayList<>();

    void showProjects() {
        if (projects.size() == 0)
            System.out.println("LIST OF PROJECTS IS EMPTY");

        for (int i = 0; i < projects.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") " + projects.get(i).toString());
        }
    }

    void addProject(String projectName) {
        projects.add(new Project(projectName));
    }

    void changeProject(int projectNumber, String newName) {
        projects.set(projectNumber - 1, new Project(newName));
    }

    void deleteProject(int projectNumber) {
        projects.remove(projectNumber - 1);
    }

    void clearProjects() {
        projects.clear();
    }

}
