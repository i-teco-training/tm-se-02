package ru.alekseev.tm;

import java.util.ArrayList;
import java.util.List;

public class TaskManager {
    private List<Task> tasks = new ArrayList<>();

    void showTasks() {
        if (tasks.size() == 0)
            System.out.println("LIST OF TASKS IS EMPTY");

        for (int i = 0; i < tasks.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") " + tasks.get(i).toString());
        }
    }

    void addTask(String taskName) {
        tasks.add(new Task(taskName));
    }

    void changeTask(int taskNumber, String newName) {
        tasks.set(taskNumber - 1, new Task(newName));
    }

    void deleteTask(int taskNumber) {
        tasks.remove(taskNumber - 1);
    }

    void clearTasks() {
        tasks.clear();
    }
}
