package ru.alekseev.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    static ProjectManager projectManager = new ProjectManager();
    static TaskManager taskManager = new TaskManager();
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        while (true) {
            String input = reader.readLine();
            switch (input) {
                case CommentConst.HELP:
                    showCommands();
                    break;
                case CommentConst.SHOW_PROJECTS:
                    showProjects();
                    break;
                case CommentConst.ADD_PROJECT:
                    addProject();
                    break;
                case CommentConst.UPDATE_PROJECT:
                    updateProject();
                    break;
                case CommentConst.DELETE_PROJECT:
                    deleteProject();
                    break;
                case CommentConst.CLEAR_PROJECTS:
                    clearProjects();
                    break;
                case CommentConst.SHOW_TASKS:
                    showTasks();
                    break;
                case CommentConst.ADD_TASK:
                    addTask();
                    break;
                case CommentConst.UPDATE_TASK:
                    updateTask();
                    break;
                case CommentConst.DELETE_TASK:
                    deleteTask();
                    break;
                case CommentConst.CLEAR_TASKS:
                    clearTasks();
                    break;
                case CommentConst.EXIT:
                    System.exit(0);
                    break;
                default:
                    showCommands();
                    break;
            }
            System.out.println();
        }
    }

    static void showCommands() {
        System.out.println(CommentConst.HELP + " - Show all commands");

        System.out.println(CommentConst.SHOW_PROJECTS + " - Show all projects");
        System.out.println(CommentConst.ADD_PROJECT + " - Add new project");
        System.out.println(CommentConst.UPDATE_PROJECT + " - Update project");
        System.out.println(CommentConst.DELETE_PROJECT + " - Delete project by number");
        System.out.println(CommentConst.CLEAR_PROJECTS + " - Delete all projects");

        System.out.println(CommentConst.SHOW_TASKS + " - Show all tasks");
        System.out.println(CommentConst.ADD_TASK + " - Add new task");
        System.out.println(CommentConst.UPDATE_TASK + " - Update task");
        System.out.println(CommentConst.DELETE_TASK + " - Delete task by number");
        System.out.println(CommentConst.CLEAR_TASKS + " - Delete all tasks");

        System.out.println(CommentConst.EXIT + " - Stop the Project Manager App");
    }

    private static void showProjects() {
        System.out.println("[LIST OF ALL PROJECTS]");
        projectManager.showProjects();
    }

    private static void addProject() throws IOException {
        System.out.println("[ADDING OF NEW PROJECT]");
        System.out.println("ENTER NAME");
        String ss = reader.readLine();
        projectManager.addProject(ss);
        System.out.println("[OK]");
    }

    private static void updateProject() throws IOException {
        System.out.println("[UPDATING OF PROJECT]");
        System.out.println("ENTER NUMBER OF PROJECT");
        int projectNumber = Integer.parseInt(reader.readLine());
        System.out.println("ENTER NEW NAME");
        String newName = reader.readLine();
        projectManager.changeProject(projectNumber, newName);
        System.out.println("[OK]");
    }

    private static void deleteProject() throws IOException {
        System.out.println("[DELETING OF PROJECT]");
        System.out.println("ENTER NUMBER OF PROJECT");
        int deleteNumber = Integer.parseInt(reader.readLine());
        projectManager.deleteProject(deleteNumber);
        System.out.println("[OK]");
    }

    private static void clearProjects() {
        projectManager.clearProjects();
        System.out.println("[ALL PROJECTS DELETED]");
    }

    private static void showTasks() {
        System.out.println("[LIST OF ALL TASKS]");
        taskManager.showTasks();
    }

    private static void addTask() throws IOException {
        System.out.println("[ADDING OF NEW TASK]");
        System.out.println("ENTER NAME");
        String str = reader.readLine();
        taskManager.addTask(str);
        System.out.println("[OK]");
    }

    private static void updateTask() throws IOException {
        System.out.println("ENTER TASK NUMBER");
        int taskNumber = Integer.parseInt(reader.readLine());
        System.out.println("ENTER NEW NAME");
        String newName2 = reader.readLine();
        taskManager.changeTask(taskNumber, newName2);
        System.out.println("[OK]");
    }

    private static void deleteTask() throws IOException {
        System.out.println("[DELETING OF TASK]");
        System.out.println("ENTER NUMBER OF TASK");
        int delNumber = Integer.parseInt(reader.readLine());
        taskManager.deleteTask(delNumber);
        System.out.println("[OK]");
    }

    private static void clearTasks() {
        taskManager.clearTasks();
        System.out.println("[ALL TASKS DELETED]");
    }
}
