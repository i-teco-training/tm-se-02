package ru.alekseev.tm;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private String name;
    private List<Task> tasks = new ArrayList<>();

    public Project(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
